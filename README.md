# what

* 学习使用composer来创建 发布php的包

## how

### 1 初始化

```
composer init --name "do365/142-composer-hello-world" --require "monolog/monolog:1.24.0" -n
```

### 2 发布

* 登录 https://packagist.org
* submit `git@gitlab.com:do365-public/142-composer-hello-world.git`

### 3 发布检查

* https://packagist.org/packages/do365/142-composer-hello-world

### 4 自动发布

* gitlab do365-public 142-composer-hello-world Settings Integrations Packagist
* 输入username token